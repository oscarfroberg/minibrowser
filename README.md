# Minibrowser 

A minimal bare-bones web browser with tabs written in Python using Webkit. Based on code from [https://gist.github.com/kklimonda/890640](https://gist.github.com/kklimonda/890640)

## Keyboard navigation

Alt-D or Ctrl-L focuses address bar. Tab navigation with Alt-1 to Alt-9 or Ctrl-Tab (right) and Ctrl-Shift-Tab (left). Ctrl-W closes tab (and browser if only one tab open).
