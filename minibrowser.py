#!/usr/bin/env python

import sys
from gi.repository import Gtk, Gdk, WebKit

class BrowserTab(Gtk.VBox):
    def __init__(self, *args, **kwargs):
        super(BrowserTab, self).__init__(*args, **kwargs)

        go_button = Gtk.Button(">>>")
        go_button.connect("clicked", self._load_url)
        self.url_bar = Gtk.Entry()
        self.url_bar.connect("activate", self._load_url)
        self.webview = WebKit.WebView()
        self.websettings = WebKit.WebSettings()
        self.show()

        self.go_back = Gtk.Button("<--")
        self.go_back.connect("clicked", lambda x: self.webview.go_back())
        self.go_forward = Gtk.Button("-->")
        self.go_forward.connect("clicked", lambda x: self.webview.go_forward())

        scrolled_window = Gtk.ScrolledWindow()
        scrolled_window.add(self.webview)

        find_box = Gtk.HBox()
        close_button = Gtk.Button("Close")
        close_button.connect("clicked", lambda x: find_box.hide())
        self.find_entry = Gtk.Entry()
        self.find_entry.connect("activate",
                                lambda x: self.webview.search_text(self.find_entry.get_text(),
                                                                   False, True, True))
        prev_button = Gtk.Button("Previous")
        next_button = Gtk.Button("Next")
        prev_button.connect("clicked",
                            lambda x: self.webview.search_text(self.find_entry.get_text(),
                                                               False, False, True))
        next_button.connect("clicked",
                            lambda x: self.webview.search_text(self.find_entry.get_text(),
                                                               False, True, True))
        find_box.pack_start(close_button, False, False, 0)
        find_box.pack_start(self.find_entry, False, False, 0)
        find_box.pack_start(prev_button, False, False, 0)
        find_box.pack_start(next_button, False, False, 0)
        self.find_box = find_box

        url_box = Gtk.HBox()
        url_box.pack_start(self.go_back, False, False, 0)
        url_box.pack_start(self.go_forward, False, False, 0)
        url_box.pack_start(self.url_bar, True, True, 0)
        url_box.pack_start(go_button, False, False, 0)

        self.pack_start(url_box, False, False, 0)
        self.pack_start(scrolled_window, True, True, 0)
        self.pack_start(find_box, False, False, 0)

        url_box.show_all()
        scrolled_window.show_all()

    def _load_url(self, widget):
        url = self.url_bar.get_text()
        if not "://" in url:
            url = "http://" + url
            self.url_bar.set_text(url)
        self.webview.load_uri(url)

class Browser(Gtk.Window):
    def __init__(self, *args, **kwargs):
        super(Browser, self).__init__(*args, **kwargs)

        # create notebook and tabs
        self.notebook = Gtk.Notebook()
        self.notebook.set_scrollable(True)

        # basic stuff
        self.tabs = []
        self.set_size_request(320,240) # minimum size of window
        self.set_default_size(1024,600) # default size of window (when un-maximized)
        #self.maximize() # maximize on open
        self.set_title("Mini Web Browser")

        # create a first, empty browser tab
        self.tabs.append((self._create_tab(), Gtk.Label("New Tab")))
        self.notebook.append_page(*self.tabs[0])
        self.add(self.notebook)

        # connect signals
        self.connect("destroy", Gtk.main_quit)
        self.connect("key-press-event", self._key_pressed)
        self.notebook.connect("switch-page", self._tab_changed)

        self.notebook.show()
        self.show()

        # focus url bar on launch
        current_page = self.notebook.get_current_page()
        self.tabs[current_page][0].url_bar.grab_focus()

    def _tab_changed(self, notebook, current_page, index):
        if not index:
            return
        title = self.tabs[index][0].webview.get_title()
        if title:
            self.set_title(title)

    def _title_changed(self, webview, frame, title):
        current_page = self.notebook.get_current_page()

        counter = 0
        for tab, label in self.tabs:
            if tab.webview is webview:
                label.set_text(title)
                if counter == current_page:
                    self._tab_changed(None, None, counter)
                break
            counter += 1

    def _create_tab(self):
        tab = BrowserTab()
        tab.webview.connect("title-changed", self._title_changed)
        return tab

    def _reload_tab(self):
        self.tabs[self.notebook.get_current_page()][0].webview.reload()

    def _close_current_tab(self):
        if self.notebook.get_n_pages() == 1:
            Gtk.main_quit()
        page = self.notebook.get_current_page()
        current_tab = self.tabs.pop(page)
        self.notebook.remove(current_tab[0])

    def _open_new_tab(self):
        current_page = self.notebook.get_current_page()
        page_tuple = (self._create_tab(), Gtk.Label("New Tab"))
        self.tabs.insert(current_page+1, page_tuple)
        self.notebook.insert_page(page_tuple[0], page_tuple[1], current_page+1)
        self.notebook.set_current_page(current_page+1)
        self._focus_url_bar()

    def _focus_url_bar(self):
        current_page = self.notebook.get_current_page()
        self.tabs[current_page][0].url_bar.grab_focus()

    def _raise_find_dialog(self):
        current_page = self.notebook.get_current_page()
        self.tabs[current_page][0].find_box.show_all()
        self.tabs[current_page][0].find_entry.grab_focus()

    def _goto_next_tab(self):
        current_page = self.notebook.get_current_page()
        self.notebook.set_current_page(current_page+1)

    def _goto_previous_tab(self):
        current_page = self.notebook.get_current_page()
        self.notebook.set_current_page(current_page-1)

    # the following is really shitty but I haven't figured out how to pass variables in this scenario yet, for some reason
    def _gototab1(self):
        self.notebook.set_current_page(0)

    def _gototab2(self):
        self.notebook.set_current_page(1)

    def _gototab3(self):
        self.notebook.set_current_page(2)

    def _gototab4(self):
        self.notebook.set_current_page(3)

    def _gototab5(self):
        self.notebook.set_current_page(4)

    def _gototab6(self):
        self.notebook.set_current_page(5)

    def _gototab7(self):
        self.notebook.set_current_page(6)

    def _gototab8(self):
        self.notebook.set_current_page(7)

    def _gototab9(self):
        self.notebook.set_current_page(8)

    def _key_pressed(self, widget, event):
        modifiers = Gtk.accelerator_get_default_mod_mask()
        mapctrl = {
                Gdk.KEY_r: self._reload_tab,
                Gdk.KEY_w: self._close_current_tab,
                Gdk.KEY_t: self._open_new_tab,
                Gdk.KEY_l: self._focus_url_bar,
                Gdk.KEY_f: self._raise_find_dialog,
                Gdk.KEY_q: Gtk.main_quit,
                Gdk.KEY_Tab: self._goto_next_tab
                }
        mapalt = {
                Gdk.KEY_d: self._focus_url_bar,
                Gdk.KEY_1: self._gototab1,
                Gdk.KEY_2: self._gototab2,
                Gdk.KEY_3: self._gototab3,
                Gdk.KEY_4: self._gototab4,
                Gdk.KEY_5: self._gototab5,
                Gdk.KEY_6: self._gototab6,
                Gdk.KEY_7: self._gototab7,
                Gdk.KEY_8: self._gototab8,
                Gdk.KEY_9: self._gototab9
                }
        mapctrlshift = {
                Gdk.KEY_ISO_Left_Tab: self._goto_previous_tab
                }
        if event.state & modifiers == Gdk.ModifierType.CONTROL_MASK and event.keyval in mapctrl:
            mapctrl[event.keyval]()
        elif event.state & modifiers == Gdk.ModifierType.MOD1_MASK and event.keyval in mapalt:
            mapalt[event.keyval]()
        elif event.state & modifiers == (Gdk.ModifierType.CONTROL_MASK | Gdk.ModifierType.SHIFT_MASK) and event.keyval in mapctrlshift:
            mapctrlshift[event.keyval]()

if __name__ == "__main__":
    Gtk.init(sys.argv)
    browser = Browser()
    Gtk.main()
